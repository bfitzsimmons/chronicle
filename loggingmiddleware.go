package chronicle

import (
	"fmt"
	"net/http"
	"time"
)

// LoggingResponseWriter wraps the response writer and adds `code` and `size` properties.
type LoggingResponseWriter struct {
	http.ResponseWriter
	code int
	size int
}

// NewLoggingResponseWriter returns a pointer to an instance of LoggingResponseWriter.
func NewLoggingResponseWriter(w http.ResponseWriter) *LoggingResponseWriter {
	return &LoggingResponseWriter{
		ResponseWriter: w,
		code:           http.StatusOK,
		size:           0,
	}
}

// WriteHeader writes the response code to the wrapped response writer.
func (lrw *LoggingResponseWriter) WriteHeader(code int) {
	lrw.code = code
	lrw.ResponseWriter.WriteHeader(code)
}

// Write gets the length of the body and writes the body to the response writer.
func (lrw *LoggingResponseWriter) Write(body []byte) (int, error) {
	lrw.size += len(body)
	return lrw.ResponseWriter.Write(body)
}

// LoggingMiddleware logs the response from the upstream handler(s).
func LoggingMiddleware(next http.Handler) http.Handler {
	// Initialize the Loggers.
	stdLogger, errLogger := InitializeLoggers()

	logFunc := func(w http.ResponseWriter, r *http.Request) {
		/*----------------------------------------------------------------------
		Request side.
		----------------------------------------------------------------------*/
		// Start the timer.
		startTime := time.Now()

		// Wrap the response and send it to the next handler in the chain.
		lrw := NewLoggingResponseWriter(w)
		next.ServeHTTP(lrw, r)

		/*----------------------------------------------------------------------
		Response side.
		----------------------------------------------------------------------*/
		// Write out the log entry.
		msg := fmt.Sprintf("%s  %d %s %s %s %s %s %d  %s",
			r.UserAgent(),
			lrw.size,
			r.Method,
			time.Since(startTime),
			r.Proto,
			r.Referer(),
			r.RemoteAddr,
			lrw.code,
			r.URL.String(),
		)

		switch {
		case lrw.code >= 500:
			errLogger.Print(msg)
		default:
			stdLogger.Print(msg)
		}
	}

	return http.HandlerFunc(logFunc)
}

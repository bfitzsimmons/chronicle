package chronicle

import (
	"log"
	"os"
)

const flags = log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile

// Logger is an interface for the most common logging methods.
type Logger interface {
	Print(v ...interface{})
	Printf(format string, v ...interface{})
	Println(v ...interface{})
	Fatal(v ...interface{})
	Fatalf(format string, v ...interface{})
	Panic(v ...interface{})
	Panicf(format string, v ...interface{})
}

// InitializeLoggers returns loggers for logging to stdout and stderr.
func InitializeLoggers() (Logger, Logger) {
	return NewStdLogger(), NewErrLogger()
}

// NewErrLogger returns a logger for logging to stderr.
func NewErrLogger() Logger {
	return log.New(os.Stderr, "", flags)
}

// NewStdLogger returns a logger for logging to stdout.
func NewStdLogger() Logger {
	return log.New(os.Stdout, "", flags)
}

package chronicle

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitializeLoggers(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		stdLogger Logger
		errLogger Logger
	}{
		{
			name:      "valid",
			stdLogger: log.New(os.Stdout, "", flags),
			errLogger: log.New(os.Stderr, "", flags),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			stdLogger, errLogger := InitializeLoggers()
			assert.IsType(t, tt.stdLogger.(Logger), stdLogger)
			assert.IsType(t, tt.errLogger.(Logger), errLogger)
		})
	}
}

func TestNewErrLogger(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		errLogger Logger
	}{
		{
			name:      "valid",
			errLogger: log.New(os.Stderr, "", flags),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			assert.IsType(t, tt.errLogger.(Logger), NewErrLogger())
		})
	}
}

func TestNewStdLogger(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		stdLogger Logger
	}{
		{
			name:      "valid",
			stdLogger: log.New(os.Stdout, "", flags),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			assert.IsType(t, tt.stdLogger.(Logger), NewStdLogger())
		})
	}
}
